<?php declare(strict_types=1);

namespace Plugin\install_template;

use JTL\License\AjaxResponse;
use JTL\License\Installer\TemplateInstaller;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;

/**
 * Class Bootstrap
 * @package Plugin\install_template
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function installed()
    {
        parent::installed();
        $templateArchive = __DIR__ . '/NOVAChild.zip';
        if (\file_exists($templateArchive)) {
            $installer = new TemplateInstaller($this->getDB(), $this->getCache());
            if ($installer->install('NOVAChild', $templateArchive, new AjaxResponse()) === 1) {
                Shop::Container()->getTemplateService()->setActiveTemplate('NOVAChild');
            }
        }
    }
}
